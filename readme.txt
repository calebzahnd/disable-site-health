=== Disable Site Health ===

Contributors: calebzahnd
Plugin Name: Disable Site Health
Tags: wp, site health, disable
Author URI: https://calebzahnd.com
Author: Caleb Zahnd
Requires at least: 5.2
Tested up to: 5.2
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Disable the Site Health pages in the admin interface that were added as part of WordPress v5.2